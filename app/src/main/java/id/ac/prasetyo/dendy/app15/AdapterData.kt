package id.ac.prasetyo.dendy.app15

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterData (val data : List<HashMap<String,String>>,
                   val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterData.HolderData>(){

    var var2 : String = ""

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterData.HolderData {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_data,p0,false)
        return HolderData(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: AdapterData.HolderData, p1: Int) {
        val dt = data.get(p1)
        p0.txNama.setText(dt.get("nama"))
        p0.txPerasaan.setText("Merasa " + dt.get("respon"))
        p0.txKeterangan.setText(dt.get("ket"))

        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            mainActivity.edNama.setText(dt.get("nama"))
            var2 = dt.get("perasaan").toString()
            if(var2 == "Sangat Puas"){
                mainActivity.rg.check(R.id.rbSP)
            }
            else if(var2 == "Puas"){
                mainActivity.rg.check(R.id.rbP)
            }else if(var2 == "Tidak Puas"){
                mainActivity.rg.check(R.id.rbTP)
            }
            mainActivity.edKet.setText(dt.get("ket"))
            mainActivity.id = dt.get("id")
            Picasso.get().load(dt.get("url")).into(mainActivity.imageView);
        })

        if(!dt.get("url").equals(""))
            Picasso.get().load(dt.get("url")).into(p0.foto);

    }

    inner class HolderData(v : View) : RecyclerView.ViewHolder(v){
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txPerasaan = v.findViewById<TextView>(R.id.txPerasaan)
        val txKeterangan = v.findViewById<TextView>(R.id.txKeterangan)
        val foto = v.findViewById<ImageView>(R.id.imageView2)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}